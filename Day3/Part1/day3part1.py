import string

def print_priorityscore():
    input_file = open("input.txt", "r")
    priorityscore_lst = []
    alphabet_by_idx = list(string.ascii_letters)

    #go through input, split strings into compartments
    for line in input_file:
        compartment_items = []
        items = line.strip()
        compartment_items.append(items[0:len(items)//2])
        compartment_items.append(items[len(items)//2:len(items)])
        common_character = ''.join(set(compartment_items[0]).intersection(compartment_items[1]))
        #get priority score from where letter appears in alphabet
        priorityscore_lst.append(alphabet_by_idx.index(common_character)+1)
    input_file.close()

    print('Sum of the priorities:', sum(priorityscore_lst))
if __name__ == '__main__':
    print_priorityscore()

