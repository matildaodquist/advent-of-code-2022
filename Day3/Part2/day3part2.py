import string

def print_priorityscore():
    input_file = open("input.txt", "r")
    groups_lst = []
    alphabet_by_idx = list(string.ascii_letters)
    group_counter = 0
    group = []
    priorityscore_lst =[]

    #go through input, divide elves into groups of three
    for line in input_file:
        items = line.strip()
        group.append(items)
        if group_counter == 2:
            group_counter = 0
            groups_lst.append(group)
            group = []
        else:
            group_counter += 1
    input_file.close()    
    
    #decide common item for each group and store priority in groups_lst
    for group in groups_lst:
        common_character = ''.join((set(group[0]).intersection(group[1])).intersection(group[2]))
        priorityscore_lst.append(alphabet_by_idx.index(common_character)+1)

    print('Sum of the priorities:', sum(priorityscore_lst))
if __name__ == '__main__':
    print_priorityscore()

