def print_score():
    input_file = open("input.txt", "r")
    list_of_points = []

    #go through input, convert letters to points
    #1 = rock, 2 = paper, 3 = scissors
    for line in input_file:
        round_moves = line.split()
        for a in range (0, len(round_moves)):
            if (round_moves[a] == 'A') or (round_moves[a] == 'X'):
                round_moves[a] = 1
            elif (round_moves[a] == 'B') or (round_moves[a] == 'Y'): 
                round_moves[a] = 2
            elif (round_moves[a] == 'C') or (round_moves[a] == 'Z'):    
                round_moves[a] = 3
            if a == 1:
                list_of_points.append(round_moves)
    input_file.close()

    #go through list of points, decide winner for each round
    for a in range (0, len(list_of_points)):
        outcome_point = 0
        if list_of_points[a][0] == list_of_points[a][1]:
            outcome_point = 3
        elif (list_of_points[a][0] == 1) and (list_of_points[a][1] != 3):
            outcome_point = 6
        elif (list_of_points[a][0] == 2) and (list_of_points[a][1] != 1):
            outcome_point = 6
        elif (list_of_points[a][0] == 3) and (list_of_points[a][1] != 2):
            outcome_point = 6
        #summarize points
        list_of_points[a] = outcome_point + list_of_points[a][1]

    print("The total score is:", sum(list_of_points))

if __name__ == '__main__':
    print_score()

