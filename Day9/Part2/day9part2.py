def print_result():
    input_file = open("input.txt", "r")
    input_lst = []
    h_index = [0, 0]
    t_indices = ([[0, 0] for _ in range(9)])
    t_hasbeen = []
    t_hasbeen.append([0, 0])
    
    #go through input to get instructions
    for line in input_file:
        stripped_line = line.strip()
        stripped_line = stripped_line.split(' ')
        input_lst.append(stripped_line)
    input_file.close()

    #go through input_lst and do all the movements for head and tail
    for line in input_lst:
        if line[0] == 'R':
            h_index, t_indices, t_hasbeen = movement(int(line[1]), h_index, t_indices, t_hasbeen, line[0])
        if line[0] == 'U':
            h_index, t_indices, t_hasbeen = movement(int(line[1]), h_index, t_indices, t_hasbeen, line[0])
        if line[0] == 'L':
            h_index, t_indices, t_hasbeen = movement(int(line[1]), h_index, t_indices, t_hasbeen, line[0])
        if line[0] == 'D':
            h_index, t_indices, t_hasbeen = movement(int(line[1]), h_index, t_indices, t_hasbeen, line[0])
    
    #list to store unique values where tail has been
    empty_list = [] 
    #counter variable to count unique values
    count = 0
    #travesing the array 
    for ele in t_hasbeen:
        if(ele not in empty_list):
            count += 1
            empty_list.append(ele) 
    #print result
    print("Number places tail part 9 has been:", count)

#helper function for movement, does movement horizontally or vertically
def movement_helper(indx, direct):
    if direct == 'R':
        indx[1] = indx[1] + 1
    elif direct == 'U':
        indx[0] = indx[0] - 1
    elif direct == 'L':
        indx[1] = indx[1] - 1
    elif direct == 'D':
        indx[0] = indx[0] + 1
    return indx

#function that is in charge of movements, updates indexes
def movement(num_steps, h_index, t_indices, t_hasbeen, direct):
    for i in range(0, num_steps):
        h_index = movement_helper(h_index, direct)
        #find out if tail needs to be moved
        for j in range(0,len(t_indices)):
            if j == 0:
                lead_index = [h_index[0], h_index[1]]
            else: #tail-part is lead
                lead_index = [t_indices[j-1][0], t_indices[j-1][1]]
            if not ((abs(lead_index[0] - t_indices[j][0]) <= 1) and (abs(lead_index[1] - t_indices[j][1]) <= 1)):
                if (abs(lead_index[0] - t_indices[j][0])+abs(lead_index[1] - t_indices[j][1]) <= 2):    
                    if lead_index[1] > t_indices[j][1]:     
                        #move tail right
                        t_indices[j] = movement_helper(t_indices[j], 'R')
                    elif lead_index[0] < t_indices[j][0]:
                        #move tail up
                        t_indices[j] = movement_helper(t_indices[j], 'U')
                    elif lead_index[1] < t_indices[j][1]:
                        #move tail left
                        t_indices[j] = movement_helper(t_indices[j], 'L')
                    elif lead_index[0] > t_indices[j][0]:
                        #move tail down
                        t_indices[j] = movement_helper(t_indices[j], 'D')
                else: #diagonal movement needed
                    if ((lead_index[1] - 2 == t_indices[j][1]) and (abs(lead_index[0] - t_indices[j][0]) <= 1)):
                        #move to spot left of lead
                        t_indices[j] = [lead_index[0],lead_index[1]-1]
                    elif ((lead_index[0] + 2 == t_indices[j][0]) and (abs(lead_index[1] - t_indices[j][1]) <= 1)):
                        #move to spot under lead
                        t_indices[j] = [lead_index[0]+1,lead_index[1]]
                    elif ((lead_index[1] + 2 == t_indices[j][1]) and (abs(lead_index[0] - t_indices[j][0]) <= 1)):
                        #move to spot right of lead
                        t_indices[j] = [lead_index[0],lead_index[1]+1]
                    elif ((lead_index[0] - 2 == t_indices[j][0]) and (abs(lead_index[1] - t_indices[j][1]) <= 1)):
                        #move to spot over lead
                        t_indices[j] = [lead_index[0]-1,lead_index[1]]
                    #lead index is far ahead
                    elif ((lead_index[0] > t_indices[j][0]) and (lead_index[1] > t_indices[j][1])):
                        #move one step down and one step right
                        t_indices[j] = [t_indices[j][0]+1, t_indices[j][1]+1]
                    elif ((lead_index[0] < t_indices[j][0]) and (lead_index[1] > t_indices[j][1])):
                        #move one step up and one step right
                        t_indices[j] = [t_indices[j][0]-1, t_indices[j][1]+1]
                    elif ((lead_index[0] > t_indices[j][0]) and (lead_index[1] < t_indices[j][1])):
                        #move one step down and one step left
                        t_indices[j] = [t_indices[j][0]+1, t_indices[j][1]-1]
                    elif ((lead_index[0] < t_indices[j][0]) and (lead_index[1] < t_indices[j][1])):
                        #move one step up and one step left
                        t_indices[j] = [t_indices[j][0]-1, t_indices[j][1]-1]
                if j == 8:
                    t_hasbeen.append([t_indices[8][0], t_indices[8][1]])
    return h_index, t_indices, t_hasbeen


if __name__ == '__main__':
    print_result() 
