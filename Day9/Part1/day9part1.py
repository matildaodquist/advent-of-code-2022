def print_result():
    input_file = open("input.txt", "r")
    input_lst = []
    h_index = [0, 0]
    t_index = [0, 0]
    t_hasbeen = []
    t_hasbeen.append([0, 0])

    #go through input to get instructions
    for line in input_file:
        stripped_line = line.strip()
        stripped_line = stripped_line.split(' ')
        input_lst.append(stripped_line)
    input_file.close()

    #go through input_lst and do all the movements for head and tail
    for line in input_lst:
        if line[0] == 'R':
            h_index, t_index, t_hasbeen = movement(int(line[1]), h_index, t_index, t_hasbeen, line[0])
        if line[0] == 'U':
            h_index, t_index, t_hasbeen = movement(int(line[1]), h_index, t_index, t_hasbeen, line[0])
        if line[0] == 'L':
            h_index, t_index, t_hasbeen = movement(int(line[1]), h_index, t_index, t_hasbeen, line[0])
        if line[0] == 'D':
            h_index, t_index, t_hasbeen = movement(int(line[1]), h_index, t_index, t_hasbeen, line[0])
    
    #list to store unique values where tail has been
    empty_list = [] 
    #counter variable to count unique values
    count = 0
    #travesing the array 
    for ele in t_hasbeen:
        if(ele not in empty_list):
            count += 1
            empty_list.append(ele) 
    #print result
    print("Number places tail has been:", count)

#helper function for movement, does movement horizontally or vertically
def movement_helper(indx, direct):
    if direct == 'R':
        indx[1] = indx[1] + 1
    elif direct == 'U':
        indx[0] = indx[0] - 1
    elif direct == 'L':
        indx[1] = indx[1] - 1
    elif direct == 'D':
        indx[0] = indx[0] + 1
    return indx

#function that is in charge of movements, updates indexes
def movement(num_steps, h_index, t_index, t_hasbeen, direct):
    for i in range(0, num_steps):
        h_index = movement_helper(h_index, direct)
        #find out if tail needs to be moved
        if not ((abs(h_index[0] - t_index[0]) <= 1) and (abs(h_index[1] - t_index[1]) <= 1)):
            if (abs(h_index[0] - t_index[0])+abs(h_index[1] - t_index[1]) <= 2):    
                if direct == 'R':
                    #move tail right
                    t_index = movement_helper(t_index, 'R')
                elif direct == 'U':
                    #move tail up
                    t_index = movement_helper(t_index, 'U')
                elif direct == 'L':
                    #move tail left
                    t_index = movement_helper(t_index, 'L')
                elif direct == 'D':
                    #move tail down
                    t_index = movement_helper(t_index, 'D')
            else: #diagonal movement needed
                if direct == 'R':
                    t_index = [h_index[0],h_index[1]-1]
                elif direct == 'U':
                    t_index = [h_index[0]+1,h_index[1]]
                elif direct == 'L':
                    t_index = [h_index[0],h_index[1]+1]
                elif direct == 'D':
                    t_index = [h_index[0]-1,h_index[1]]
            t_hasbeen.append([t_index[0], t_index[1]])
    return h_index, t_index, t_hasbeen


if __name__ == '__main__':
    print_result() 
