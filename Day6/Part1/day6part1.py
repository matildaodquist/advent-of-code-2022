def print_result():
    input_file = open("input.txt", "r")
    datastream = ''
    last_idx = 0

    #put input in string
    for line in input_file:
        datastream = line.strip()
    input_file.close()

    #go through input, in sequence of four unique characters
    for a in range(0, len(datastream)):
        if not has_repeated_chars(datastream[a:a+4]):
            last_idx = a + 4
            break

    print("Characters needed to be processed before start-of-packet marker:", last_idx)

#helper function to decide if a string has duplicate characters
def has_repeated_chars(string):
    for char in string:
        if string.count(char) > 1:
            return True
    return False


if __name__ == '__main__':
    print_result()


