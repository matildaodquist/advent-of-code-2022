def print_calories():
    input_file = open("input.txt", "r")
    list_of_nums = []
    elf_num = 0

    #categorize each elf and their baggage in list of lists
    list_of_nums.append(['elf: '+str(elf_num)])
    for line in input_file:
        stripped_line = line.strip()
        if stripped_line != '':
            list_of_nums[elf_num].append(stripped_line)        
        else:
            total = 0
            for a in range(1, len(list_of_nums[elf_num])):
                total += int(list_of_nums[elf_num][a])
            list_of_nums[elf_num].append(total)   
            elf_num += 1
            list_of_nums.append(['elf: '+str(elf_num)])
    input_file.close()

    #do last elf separately, since no '' after its baggage
    total = 0
    for a in range(1, len(list_of_nums[elf_num])):
        total += int(list_of_nums[elf_num][a])
    list_of_nums[elf_num].append(total)

    #find out what three elves carry the most calories
    most_idx = 0
    second_most_idx = 0
    third_most_idx = 0
    for elf in range(0, len(list_of_nums)):
        if list_of_nums[elf][-1] > list_of_nums[most_idx][-1]:
            third_most_idx = second_most_idx
            second_most_idx = most_idx
            most_idx = elf
        elif list_of_nums[elf][-1] > list_of_nums[second_most_idx][-1]:
            third_most_idx = second_most_idx
            second_most_idx = elf
        elif list_of_nums[elf][-1] > list_of_nums[third_most_idx][-1]:
            third_most_idx = elf
    
    print('Elf carrying the most baggage is', list_of_nums[most_idx][0],
            '\nElf carrying the seond most baggage is', list_of_nums[second_most_idx][0],
            '\nElf carrying the third most baggage is', list_of_nums[third_most_idx][0],
            '\nNumber of calories they carry in total:', 
            list_of_nums[most_idx][-1]+list_of_nums[second_most_idx][-1]+list_of_nums[third_most_idx][-1])

if __name__ == '__main__':
    print_calories()
