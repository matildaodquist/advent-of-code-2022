import numpy as np

def print_result():
    input_file = open("input.txt", "r")
    inp_mtrx = []
    score_mtrx = []
    score_mtrx_mult = []

    #go through input and append to list
    for line in input_file:
        inp_line = []
        stripped_line = line.strip()
        for i in range(0, len(stripped_line)):
            inp_line.append(int(stripped_line[i]))
        inp_mtrx.append(inp_line)
    input_file.close()
   
    #make matrix to keep track of scenic scores (order: above, left, below, right) 
    for i in range(0, len(inp_mtrx)):
        score_mtrx.append([[0, 0, 0, 0] for _ in range(len(inp_mtrx[0]))])

    #make matrix to keep track of scenic scores, multiplied by each direction
    for i in range(0, len(inp_mtrx)):
        score_mtrx_mult.append([0 for _ in range(len(inp_mtrx[0]))])

    for x in range(0, len(inp_mtrx)):
        for y in range(0, len(inp_mtrx[0])):
            #consider trees above
            if x != 0: #make sure it is not a top edge tree
                if inp_mtrx[x][y] <= inp_mtrx[x-1][y]: #first tree covers the rest
                    score_mtrx[x][y][0] += 1
                else:
                    score_mtrx[x][y][0] += 1
                    n = 1
                    while x-n != 0:
                        next_treetop = inp_mtrx[x-1-n][y]
                        if next_treetop < inp_mtrx[x][y]:
                            score_mtrx[x][y][0] += 1
                            n += 1
                        else:
                            score_mtrx[x][y][0] += 1
                            break
            #consider trees to the left
            if y != 0: #make sure it is not a left edge tree
                if inp_mtrx[x][y] <= inp_mtrx[x][y-1]: #first tree covers the rest
                    score_mtrx[x][y][1] += 1
                else:
                    score_mtrx[x][y][1] += 1
                    n = 1
                    while y-n != 0:
                        next_treetop = inp_mtrx[x][y-1-n]
                        if next_treetop < inp_mtrx[x][y]:
                            score_mtrx[x][y][1] += 1
                            n += 1
                        else:
                            score_mtrx[x][y][1] += 1
                            break
            #consider trees below
            if x != len(inp_mtrx)-1: #make sure it is not a bottom edge tree
                if inp_mtrx[x][y] <= inp_mtrx[x+1][y]: #first tree covers the rest
                    score_mtrx[x][y][2] += 1
                else:
                    score_mtrx[x][y][2] += 1
                    n = 1
                    while x+n != len(inp_mtrx)-1:
                        next_treetop = inp_mtrx[x+1+n][y]
                        if next_treetop < inp_mtrx[x][y]:
                            score_mtrx[x][y][2] += 1
                            n += 1
                        else:
                            score_mtrx[x][y][2] += 1
                            break
            #consider trees to the right
            if y != len(inp_mtrx[0])-1: #make sure it is not a right edge tree
                if inp_mtrx[x][y] <= inp_mtrx[x][y+1]: #first tree covers the rest
                    score_mtrx[x][y][3] += 1
                else:
                    score_mtrx[x][y][3] += 1
                    n = 1
                    while y+n != len(inp_mtrx[0])-1:
                        next_treetop = inp_mtrx[x][y+1+n]
                        if next_treetop < inp_mtrx[x][y]:
                            score_mtrx[x][y][3] += 1
                            n += 1
                        else:
                            score_mtrx[x][y][3] += 1
                            break

    #do mutliplication of scenic scores
    for x in range(0, len(score_mtrx)):
        for y in range(0, len(score_mtrx[0])):
            score_mtrx_mult[x][y] = (score_mtrx[x][y][0] * score_mtrx[x][y][1] * 
                    score_mtrx[x][y][2] * score_mtrx[x][y][3])
    
    #print best scenic score
    np_score_mtrx_mult = np.matrix(score_mtrx_mult)
    max_score = np_score_mtrx_mult.max()
    print("Highest score possible is:", max_score)

if __name__ == '__main__':
    print_result()

