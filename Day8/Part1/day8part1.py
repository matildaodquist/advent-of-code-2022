def print_result():
    input_file = open("input.txt", "r")
    inp_mtrx = []
    visibility_mtrx = []

    #go through input and append to list
    for line in input_file:
        inp_line = []
        stripped_line = line.strip()
        for i in range(0, len(stripped_line)):
            inp_line.append(int(stripped_line[i]))
        inp_mtrx.append(inp_line)
    input_file.close()
   
    #make matrix to keep track of visible trees 
    for i in range(0, len(inp_mtrx)):
        visibility_mtrx.append(['n' for _ in range(len(inp_mtrx[0]))])

    for x in range(1, len(inp_mtrx[0])-1):
        for y in range(1, len(inp_mtrx)-1):
            #element is bigger than all other element to the left
            max_treetop = max(inp_mtrx[x][0:y])
            if inp_mtrx[x][y] > max_treetop:
                visibility_mtrx[x][y] = 'v'
            #element is bigger that all other element to the right
            max_treetop = max(inp_mtrx[x][y+1:])
            if inp_mtrx[x][y] > max_treetop:
                visibility_mtrx[x][y] = 'v'
            #element is bigger that all other element above
            temp_lst = []
            for i in range(0, len(inp_mtrx)):
                temp_lst.append(inp_mtrx[i][y])
            max_treetop = max(temp_lst[0:x])
            if inp_mtrx[x][y] > max_treetop:
                visibility_mtrx[x][y] = 'v'
            #element is bigger that all other element below
            max_treetop = max(temp_lst[x+1:])
            if inp_mtrx[x][y] > max_treetop:
                visibility_mtrx[x][y] = 'v'

    #count all the v's in visibility matrix
    v_count = 0
    for lsts in visibility_mtrx:
        v_count += lsts.count('v')

    #add edges to v_count
    v_count += len(inp_mtrx[0])*2 + len(inp_mtrx)*2 - 4

    print("Trees visible from outside the grid:", v_count)

if __name__ == '__main__':
    print_result()


