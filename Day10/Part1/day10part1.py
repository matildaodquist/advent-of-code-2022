def print_result():
    input_file = open("input.txt", "r")
    input_lst = ['NA'] #register already has value at start 
    x_at_cycles = [1]

    #go through input to get instructions
    for line in input_file:
        stripped_line = line.strip()
        stripped_line = stripped_line.split(' ')
        input_lst.append(stripped_line)
    input_file.close()

    #make list to keep track of future additions
    future_adds = ([0 for _ in range(len(input_lst)+4)])

    for i in range(0, len(input_lst)):
        if input_lst[i][0] == 'noop':
            x_at_cycles.append(x_at_cycles[-1]+future_adds[i])
        elif input_lst[i][0] == 'addx':
            x_at_cycles.append(x_at_cycles[-1]+future_adds[i])
            x_at_cycles.append(x_at_cycles[-1])
            future_adds[i+1] = int(input_lst[i][1])

    print("Sum of signal strengths:", x_at_cycles[20]*20 + x_at_cycles[60]*60 + 
            x_at_cycles[100]*100 + x_at_cycles[140]*140 + x_at_cycles[180]*180 + 
            x_at_cycles[220]*220)

if __name__ == '__main__':
    print_result()

