def print_result():
    input_file = open("input.txt", "r")
    input_lst = ['NA'] #register already has value at start 
    x_at_cycles = [1]

    #go through input to get instructions
    for line in input_file:
        stripped_line = line.strip()
        stripped_line = stripped_line.split(' ')
        input_lst.append(stripped_line)
    input_file.close()

    #make list to keep track of future additions
    future_adds = ([0 for _ in range(len(input_lst)+4)])

    for i in range(0, len(input_lst)):
        if input_lst[i][0] == 'noop':
            x_at_cycles.append(x_at_cycles[-1]+future_adds[i])
        elif input_lst[i][0] == 'addx':
            x_at_cycles.append(x_at_cycles[-1]+future_adds[i])
            x_at_cycles.append(x_at_cycles[-1])
            future_adds[i+1] = int(input_lst[i][1])

    #make CRT, as part of part 2
    crt_mtrx = []
    for i in range(0, int((len(x_at_cycles)-1)/40)):
        crt_mtrx.append('.' * 40)

    for n in range(0, len(crt_mtrx)):
        for i in range(1+(40*n), 41+(40*n)):
            if (x_at_cycles[i]+(40*n)) in [i-2, i-1, i]:
                tmp_lst = list(crt_mtrx[n])
                tmp_lst[(i-(40*n))-1] = '#'
                crt_mtrx[n] = tmp_lst
                crt_mtrx[n] = "".join(crt_mtrx[n])

    print("CRT-SCRREN SHOWS:")
    for lines in crt_mtrx:
        print(lines)

if __name__ == '__main__':
    print_result()

