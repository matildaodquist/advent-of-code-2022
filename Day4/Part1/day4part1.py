def print_result():
    input_file = open("input.txt", "r")
    section_duplicates = 0

    #go through input, split strings into compartments
    for line in input_file:
        stripped_line = line.strip()
        sections = stripped_line.split(',')
        section_one = sections[0].split('-') 
        section_two = sections[1].split('-')
        #arrange in lists, each list contains all sections IDs
        section_one_nums = list(range(int(section_one[0]), int(section_one[1])+1))
        section_two_nums = list(range(int(section_two[0]), int(section_two[1])+1))
        #se if one list contains all the section IDs of the other list
        if (all(item in section_one_nums for item in section_two_nums) or 
                all(item in section_two_nums for item in section_one_nums)):
            section_duplicates += 1     
    input_file.close()

    print("Assignment pairs where  one range fully contains the other:", section_duplicates)


if __name__ == '__main__':
    print_result()
