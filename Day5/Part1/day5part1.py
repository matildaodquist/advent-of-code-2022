import numpy as np

def print_result():
    input_file = open("input.txt", "r")
    inp_lst = []
    moves = []
    stacks_lst = []

    #go through input and append to list
    for line in input_file:
        if line != '':
            inp_lst.append(line) 
    input_file.close()
    
    #parsing the input...
    for strings in inp_lst:
        if strings[0] == 'm':
            moves.append([int(s) for s in strings.split() if s.isdigit()])
        else:
            split_string = [strings[i+1:i+2] for i in range(0, len(strings), 4)]
            if split_string != ['']:
                stacks_lst.append(split_string)
   
    #flip and transpose crates array
    stacks_lst = stacks_lst[0:-1]
    stacks_lst_ft = np.array(stacks_lst)
    stacks_lst_ft = np.flip(stacks_lst_ft, 0)
    stacks_lst_ft = stacks_lst_ft.transpose()
    stacks_lst_ft = stacks_lst_ft.tolist()
    
    #move forward crates, empty spots in start of list
    for d in range(0, len(stacks_lst_ft[0])):
        for c in range(0, len(stacks_lst_ft)):
            if stacks_lst_ft[c][-1] == ' ':
                stacks_lst_ft[c].insert(0, stacks_lst_ft[c].pop(-1))

    #loop through moves and do rearrangements
    for a in range(0, len(moves)):
        for b in range(0, moves[a][0]):
            popped_crate = stacks_lst_ft[moves[a][1]-1].pop()
            stacks_lst_ft[moves[a][2]-1].append(popped_crate)

    #get top crates
    top_crates = ''
    for piles in stacks_lst_ft:
        top_crates = top_crates + piles[-1]

    print("Top crates:", top_crates)


if __name__ == '__main__':
    print_result()

