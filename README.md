# Advent of code 2022

These are my solutions for advent of code 2022. Written in Python. Python 3.8.10 was used for compilation. I was unsure what programming language to use, but decided in the end to go for Python for the second year in a row. Numpy version 1.23.5 was used.

## Bash loop used to create the folders
```
for i in {1..25}; do mkdir Day${i}; mkdir Day${i}/Part1; mkdir Day${i}/Part2; touch Day${i}/Part1/day${i}part1.py; touch Day${i}/Part2/day${i}part2.py; chmod 775 Day${i}/Part1/day${i}part1.py; chmod 775 Day${i}/Part2/day${i}part2.py; done
```
